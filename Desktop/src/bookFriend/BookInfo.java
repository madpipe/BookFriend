package bookFriend;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class BookInfo implements ActionListener {
	
	public static JFrame jFbook;	
	JPanel mainPanel, bookData, buttons;
	JButton saveInfo, editFields, closeInfo, cancelEditFields;
	String[] bookLabel = {"", "ID", "Title", "Author", "Publisher", "Edition", "Print Date", "ISBN", "Status"};
	JTextField[] textField = new JTextField[] {
			new JTextField(),//CheckBok - not used here
			new JTextField(),//ID 
			new JTextField(),//Name 
			new JTextField(),//Author
			new JTextField(),//Publisher
			new JTextField(),//Edition
			new JTextField(),//Date Published / Year
			new JTextField(),//ISBN
			};
	String[] bookStatus = {"Stored", "Borrowed", "On loan"};
	Boolean name, author, publisher, edition, year, isbn;
	@SuppressWarnings("rawtypes")
	JComboBox bookStatusSelect = extracted();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox extracted() {
		return new JComboBox(bookStatus);
	}
	boolean editableFields = false;
	public String bID, bTitle;
	public String[] bookFields = new String[10];
	static MainWindow mainWindow = new MainWindow();
	
	int labelHight = 25;
	int textFheight = 25;
	
	public JPanel createContentPane(){
		
		//Creation of the main Panel-----------------------------------------------------
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		
		//Window Title background----------------------------------------------------------
		
		JPanel panel3 = new JPanel();
		panel3.setOpaque(false);
		panel3.setBounds(0, 5, 350, 40);
		mainPanel.add(panel3);
		
		JLabel fTitle = new JLabel("<html><h2>"+ bookFields[2] +"</h2></html>");
		fTitle.setHorizontalAlignment(0);
		fTitle.setForeground(Color.white);
		panel3.add(fTitle);
		
		JPanel panel2 = new JPanel();
		panel2.setBackground(new Color(87, 197, 246));
		panel2.setBounds(0, 18, 350, 12);
		mainPanel.add(panel2);
		
		JPanel panel1 = new JPanel();
		panel1.setBackground(new Color(45, 172, 242));
		panel1.setBounds(0, 15, 350, 30);
		mainPanel.add(panel1);
		
		//Creation of the Data Completion Form Panel----------------------------------------
		bookData = new JPanel();
		bookData.setLayout(null);
		bookData.setBounds(10, 45, 355, 225);
		//bookData.setBackground(Color.white);
		mainPanel.add(bookData);
		
		
		JLabel book2 = new JLabel(bookLabel[2]);
        book2.setBounds(5, 5, 100, labelHight);
        book2.setHorizontalAlignment(2);
        bookData.add(book2);
        
        textField[2].setBounds(15, 30, 300, textFheight);
        textField[2].setText(bookFields[2]);
        textField[2].setEditable(false);
        bookData.add(textField[2]);
        
		JLabel book3 = new JLabel(bookLabel[3]);
        book3.setBounds(5, 60, 100, labelHight);
        book3.setHorizontalAlignment(2);
        bookData.add(book3);
        
        textField[3].setBounds(15, 85, 150, textFheight);
        textField[3].setText(bookFields[3]);
        textField[3].setEditable(false);
        bookData.add(textField[3]);

		JLabel book4 = new JLabel(bookLabel[4]);
        book4.setBounds(190, 60, 100, labelHight);
        book4.setHorizontalAlignment(2);
        bookData.add(book4);
        
        textField[4].setBounds(200, 85, 150, textFheight);
        textField[4].setText(bookFields[4]);
        textField[4].setEditable(false);
        bookData.add(textField[4]);
        
		JLabel book5 = new JLabel(bookLabel[5]);
        book5.setBounds(5, 115, 100, labelHight);
        book5.setHorizontalAlignment(2);
        bookData.add(book5);
        
        textField[5].setBounds(15, 140, 150, textFheight);
        textField[5].setText(bookFields[5]);
        textField[5].setEditable(false);
        bookData.add(textField[5]);
        
		JLabel book6 = new JLabel(bookLabel[6]);
        book6.setBounds(190, 115, 100, labelHight);
        book6.setHorizontalAlignment(2);
        bookData.add(book6);
        
        textField[6].setBounds(200, 140, 150, textFheight);
        textField[6].setText(bookFields[6]);
        textField[6].setEditable(false);
        bookData.add(textField[6]);
        
		JLabel book7 = new JLabel(bookLabel[7]);
        book7.setBounds(5, 170, 100, labelHight);
        book7.setHorizontalAlignment(2);
        bookData.add(book7);
        
        textField[7].setBounds(15, 195, 200, textFheight);
        textField[7].setText(bookFields[7]);
        textField[7].setEditable(false);
        bookData.add(textField[7]);
        
		JLabel book8 = new JLabel(bookLabel[8]);
        book8.setBounds(240, 170, 100, labelHight);
        book8.setHorizontalAlignment(2);
        bookData.add(book8);
        
        int status = 0;
        if (bookFields[8] == "Stored")
        	status = 0;
        if (bookFields[8] == "Borrowed")
        	status = 1;
        if (bookFields[8] == "On loan")
        	status = 2;
        
        bookStatusSelect.setSelectedIndex(status);
        bookStatusSelect.setBounds(250, 195, 100, textFheight);
        bookStatusSelect.setEnabled(false);
        bookData.add(bookStatusSelect);
        
        //Creation of the Buttons Panel----------------------------------------------------------------
        buttons = new JPanel();
        buttons.setLayout(null);
        buttons.setBounds(10, 290, 455, 40);
        //buttons.setBackground(Color.white);
        mainPanel.add(buttons);
                
		saveInfo = new JButton("Save");
		saveInfo.setBounds(0, 5, 105, 25);
		saveInfo.addActionListener(this);
		buttons.add(saveInfo);
		saveInfo.setVisible(false);
		
		editFields = new JButton("Edit");
		editFields.setBounds(115, 5, 125, 25);
		editFields.addActionListener(this);
		buttons.add(editFields);
		
		cancelEditFields = new JButton("Cancel Edit");
		cancelEditFields.setBounds(115, 5, 125, 25);
		cancelEditFields.addActionListener(this);
		buttons.add(cancelEditFields);
		cancelEditFields.setVisible(false);
		
		closeInfo = new JButton("Close");
		closeInfo.setBounds(250, 5, 105, 25);
		closeInfo.addActionListener(this);
		buttons.add(closeInfo);
		
		return mainPanel;
	}
	
//############-----------------------------------------------------------------------------
	
	public BookInfo() {
		jFbook = new JFrame();
		
		//bID = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 1);
		bookFields[2] = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 2);
		bookFields[3] = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 3);
		bookFields[4] = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 4);
		bookFields[5] = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 5);
		bookFields[6] = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 6);
		bookFields[7] = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 7);
		bookFields[8] = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 8);
		jFbook.setTitle("Book Info - " + bookFields[2]);// + " - " + bID);
		
		jFbook.setLocation(0, 0);
		jFbook.setSize(390, 370);
		jFbook.setResizable(false);
		

		jFbook.setContentPane(createContentPane());
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = jFbook.getSize().width;
		int h = jFbook.getSize().height;
		int x = (dim.width-w)/2;
		int y = (dim.height-h)/2;
		jFbook.setLocation(x, y);	
		
		jFbook.setVisible(true);

	}
	
//############-----------------------------------------------------------------------------	

	public void fieldsTest() {

	}
		
//############-----------------------------------------------------------------------------	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		
		if (e.getSource() == saveInfo) {
			

			/*	Object [] row = new Object[9];
				row[0] = new Boolean(false) ;
				row[2] = textField[2].getText();
				row[3] = textField[3].getText();
				row[4] = textField[4].getText();
				row[5] = textField[5].getText();
				row[6] = textField[6].getText();
				row[7] = textField[7].getText();
				row[8] = bookStatusSelect.getSelectedItem().toString();
				
			
			MainWindow.addNewRow(row);*/
			
			
//-----------------------------------------------------------------------------------------------

			DefaultTableModel model = (DefaultTableModel) MainWindow.table.getModel();
			model.setValueAt(textField[2].getText(), MainWindow.selectedRow, 2);
			model.setValueAt(textField[3].getText(), MainWindow.selectedRow, 3);
			model.setValueAt(textField[4].getText(), MainWindow.selectedRow, 4);
			model.setValueAt(textField[5].getText(), MainWindow.selectedRow, 5);
			model.setValueAt(textField[6].getText(), MainWindow.selectedRow, 6);
			model.setValueAt(textField[7].getText(), MainWindow.selectedRow, 7);
			model.setValueAt(textField[2].getText(), MainWindow.selectedRow, 2);

			
			jFbook.setVisible(false);
		}
		else
			
		if (e.getSource() == closeInfo) {
			jFbook.setVisible(false);
		}
		if (e.getSource() == editFields || e.getSource() == cancelEditFields) {
			if (editableFields == false){
				editableFields = true;
				textField[2].setEditable(true);
				textField[3].setEditable(true);
				textField[4].setEditable(true);
				textField[5].setEditable(true);
				textField[6].setEditable(true);
				textField[7].setEditable(true);
		        bookStatusSelect.setEnabled(true);

				saveInfo.setVisible(true);
				editFields.setVisible(false);
				cancelEditFields.setVisible(true);

			}
			else if (editableFields == true){

				editableFields = false;
				textField[2].setEditable(false);
				textField[3].setEditable(false);
				textField[4].setEditable(false);
				textField[5].setEditable(false);
				textField[6].setEditable(false);
				textField[7].setEditable(false);
		        bookStatusSelect.setEnabled(false);
				saveInfo.setVisible(false);
				cancelEditFields.setVisible(false);
				editFields.setVisible(true);

			}
		}
		
	}
}
