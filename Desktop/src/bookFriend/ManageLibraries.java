package bookFriend;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class ManageLibraries {
	
	public static JFrame jFaddBook;	
	
	
	public JPanel createContentPane(){
		
		//Creation of the main Panel-----------------------------------------------------
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(null);
		
		return mainPanel;
	}
	
//############-----------------------------------------------------------------------------
	
	public ManageLibraries() {
		jFaddBook = new JFrame("Manage your libraries");
		jFaddBook.setLocation(0, 0);
		jFaddBook.setSize(390, 370);
		jFaddBook.setResizable(false);
		

		jFaddBook.setContentPane(createContentPane());
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = jFaddBook.getSize().width;
		int h = jFaddBook.getSize().height;
		int x = (dim.width-w)/2;
		int y = (dim.height-h)/2;
		jFaddBook.setLocation(x, y);	
		
		jFaddBook.setVisible(true);
		jFaddBook.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				//MainWindow.addBook = null;
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				MainWindow.addBook = null;
				
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				MainWindow.addBook = null;
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});

	}
}
