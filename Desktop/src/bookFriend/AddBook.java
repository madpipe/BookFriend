package bookFriend;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddBook implements ActionListener {
	
	public static JFrame jFaddBook;	
	JPanel mainPanel, bookData, buttons;
	JButton addButton, clearData, cancelAdd;
	String[] addBookLabel = {"", "ID", "Title", "Author", "Publisher", "Edition", "Print Date", "ISBN", "Status"};
	JTextField[] textField = new JTextField[] {
			new JTextField(),//CheckBok - not used here
			new JTextField(),//ID 
			new JTextField(),//Name 
			new JTextField(),//Author
			new JTextField(),//Publisher
			new JTextField(),//Edition
			new JTextField(),//Date Published / Year
			new JTextField(),//ISBN
			};
	String[] bookStatus = {"Stored", "Borrowed", "On loan"};
	Boolean name, author, publisher, edition, year, isbn;
	@SuppressWarnings("rawtypes")
	JComboBox bookStatusSelect = extracted();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox extracted() {
		return new JComboBox(bookStatus);
	}
	
	int labelHight = 25;
	int textFheight = 25;
	
	public JPanel createContentPane(){
		
		//Creation of the main Panel-----------------------------------------------------
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		
		//Window Title background----------------------------------------------------------
		
		JPanel panel3 = new JPanel();
		panel3.setOpaque(false);
		panel3.setBounds(0, 5, 350, 40);
		mainPanel.add(panel3);
		
		JLabel fTitle = new JLabel("<html><h2>Add a new book to your library</h2></html>");
		fTitle.setHorizontalAlignment(0);
		fTitle.setForeground(Color.white);
		panel3.add(fTitle);
		
		JPanel panel2 = new JPanel();
		panel2.setBackground(new Color(87, 197, 246));
		panel2.setBounds(0, 18, 350, 12);
		mainPanel.add(panel2);
		
		JPanel panel1 = new JPanel();
		panel1.setBackground(new Color(45, 172, 242));
		panel1.setBounds(0, 15, 350, 30);
		mainPanel.add(panel1);
		
		//Creation of the Data Completion Form Panel----------------------------------------
		bookData = new JPanel();
		bookData.setLayout(null);
		bookData.setBounds(10, 45, 355, 225);
		//bookData.setBackground(Color.white);
		mainPanel.add(bookData);
		
		
		JLabel book2 = new JLabel(addBookLabel[2]);
        book2.setBounds(5, 5, 100, labelHight);
        book2.setHorizontalAlignment(2);
        bookData.add(book2);
        
        textField[2].setBounds(15, 30, 300, textFheight);
        textField[2].setText("");
        bookData.add(textField[2]);
        
		JLabel book3 = new JLabel(addBookLabel[3]);
        book3.setBounds(5, 60, 100, labelHight);
        book3.setHorizontalAlignment(2);
        bookData.add(book3);
        
        textField[3].setBounds(15, 85, 150, textFheight);
        textField[3].setText("");
        bookData.add(textField[3]);

		JLabel book4 = new JLabel(addBookLabel[4]);
        book4.setBounds(190, 60, 100, labelHight);
        book4.setHorizontalAlignment(2);
        bookData.add(book4);
        
        textField[4].setBounds(200, 85, 150, textFheight);
        textField[4].setText("");
        bookData.add(textField[4]);
        
		JLabel book5 = new JLabel(addBookLabel[5]);
        book5.setBounds(5, 115, 100, labelHight);
        book5.setHorizontalAlignment(2);
        bookData.add(book5);
        
        textField[5].setBounds(15, 140, 150, textFheight);
        textField[5].setText("");
        bookData.add(textField[5]);
        
		JLabel book6 = new JLabel(addBookLabel[6]);
        book6.setBounds(190, 115, 100, labelHight);
        book6.setHorizontalAlignment(2);
        bookData.add(book6);
        
        textField[6].setBounds(200, 140, 150, textFheight);
        textField[6].setText("");
        bookData.add(textField[6]);
        
		JLabel book7 = new JLabel(addBookLabel[7]);
        book7.setBounds(5, 170, 100, labelHight);
        book7.setHorizontalAlignment(2);
        bookData.add(book7);
        
        textField[7].setBounds(15, 195, 200, textFheight);
        textField[7].setText("");
        bookData.add(textField[7]);
        
		JLabel book8 = new JLabel(addBookLabel[8]);
        book8.setBounds(240, 170, 100, labelHight);
        book8.setHorizontalAlignment(2);
        bookData.add(book8);
        
        bookStatusSelect.setSelectedIndex(0);
        bookStatusSelect.setBounds(250, 195, 100, textFheight);
        bookData.add(bookStatusSelect);
        
        //Creation of the Buttons Panel----------------------------------------------------------------
        buttons = new JPanel();
        buttons.setLayout(null);
        buttons.setBounds(10, 290, 455, 40);
        //buttons.setBackground(Color.white);
        mainPanel.add(buttons);
                
		addButton = new JButton("Add Book");
		addButton.setBounds(0, 5, 105, 25);
		addButton.addActionListener(this);
		buttons.add(addButton);
		
		clearData = new JButton("Clear Fields");
		clearData.setBounds(115, 5, 125, 25);
		clearData.addActionListener(this);
		buttons.add(clearData);
		
		cancelAdd = new JButton("Cancel");
		cancelAdd.setBounds(250, 5, 105, 25);
		cancelAdd.addActionListener(this);
		buttons.add(cancelAdd);
		
		return mainPanel;
	}
	
//############-----------------------------------------------------------------------------
	
	public AddBook() {
		jFaddBook = new JFrame("Add a new book");
		jFaddBook.setLocation(0, 0);
		jFaddBook.setSize(390, 370);
		jFaddBook.setResizable(false);
		

		jFaddBook.setContentPane(createContentPane());
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = jFaddBook.getSize().width;
		int h = jFaddBook.getSize().height;
		int x = (dim.width-w)/2;
		int y = (dim.height-h)/2;
		jFaddBook.setLocation(x, y);	
		
		jFaddBook.setVisible(true);
		jFaddBook.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				//MainWindow.addBook = null;
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				MainWindow.addBook = null;
				
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				MainWindow.addBook = null;
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});

	}
	
//############-----------------------------------------------------------------------------	

	public void fieldsTest() {

	}
		
//############-----------------------------------------------------------------------------	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		
		if (e.getSource() == addButton) {
			

				Object [] row = new Object[9];
				row[0] = new Boolean(false) ;
				row[1] = "";
				row[2] = textField[2].getText();
				row[3] = textField[3].getText();
				row[4] = textField[4].getText();
				row[5] = textField[5].getText();
				row[6] = textField[6].getText();
				row[7] = textField[7].getText();
				row[8] = bookStatusSelect.getSelectedItem().toString();
				
			
			MainWindow.addNewRow(row);
			jFaddBook.dispose();
		}
		else
			
		if (e.getSource() == cancelAdd) {
			//jFaddBook.setVisible(false);
			jFaddBook.dispose();
		}
		if (e.getSource() == clearData) {
			textField[2].setText("");
			textField[3].setText("");
			textField[4].setText("");
			textField[5].setText("");
			textField[6].setText("");
			textField[7].setText("");
			bookStatusSelect.setSelectedIndex(0);
		}
		
	}
}
