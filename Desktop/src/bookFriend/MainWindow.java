package bookFriend;

import java.awt.*;
import java.awt.event.*;


import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;


import javax.swing.table.TableColumn;


public class MainWindow {



	public static JFrame frame;
	public static JScrollPane scrollPane;
	public static DefaultTableModel tableModel;
    public static JTable table;
	public static int selectedRow;
	public static int columnIndexer;
	public static double versionNR = 0.3;
	public static JButton bookRead;
	static AddBook addBook;
	static BookInfo bInfo;
	static ManageLibraries manageLib;
    

	
	static void showButtons(){
		
		JButton libraryManage = new JButton(new ImageIcon("images/library.png"));
		libraryManage.setBounds(5, 5, 85, 50);
		libraryManage.setToolTipText("Manage your libraries");
		libraryManage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (manageLib == null){
					manageLib = new ManageLibraries();
				}
				else
					ManageLibraries.jFaddBook.requestFocus();
			
			}
		});
		frame.add(libraryManage);
		
		
		JButton bookAdd = new JButton(new ImageIcon("images/book_add.png"));
		bookAdd.setBounds(5, 57, 85, 50);
		bookAdd.setToolTipText("Add a new book to this library");
		bookAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				if (addBook == null){
					addBook = new AddBook();
					
				}
				else
					AddBook.jFaddBook.requestFocus();
			}
		});
		frame.add(bookAdd);
		
		JButton bookBorrow = new JButton(new ImageIcon("images/book_borrow.png"));
		bookBorrow.setBounds(5, 109, 85, 50);
		bookBorrow.setToolTipText("Borrow a book from someone");
		frame.add(bookBorrow);
		
		JButton bookLend = new JButton(new ImageIcon("images/book_lend.png"));
		bookLend.setBounds(5, 161, 85, 50);
		bookLend.setToolTipText("Lend a book to someone");
		frame.add(bookLend);
		
		bookRead = new JButton(new ImageIcon("images/book_open.png"));
		bookRead.setBounds(5, 213, 85, 50);
		bookRead.setToolTipText("View Book Information");
		bookRead.setEnabled(false);
		bookRead.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				bInfo = new BookInfo();
			}
		});

		frame.add(bookRead);
		
		/*JButton bookShowSelected = new JButton("<html><center>Show<br>chosen<br>Books</center></html>");
		bookShowSelected.setBounds(5, 290, 85, 50);
		frame.add(bookShowSelected);*/
		
		JButton bookDelete = new JButton(new ImageIcon("images/book_delete.png"));
		bookDelete.setBounds(5, 410, 85, 50);
		bookDelete.setToolTipText("Delete selected books");
		
		  bookDelete.addActionListener(new ActionListener() {
		  		@Override
		  		public void actionPerformed(ActionEvent arg0) {
		  			DefaultTableModel model = (DefaultTableModel) table.getModel();
		  			int numRows = table.getSelectedRows().length; 
		  			for(int i=0; i<numRows ; i++ ) model.removeRow(table.getSelectedRow());
		 		}
		
		  });
		 
		frame.add(bookDelete);
		
		String[] libraries = { "Home", "Office", "Workshop", "Mother's house", "School" };

		@SuppressWarnings({ "rawtypes", "unchecked" })
		JComboBox libraryList = new JComboBox(libraries);
		libraryList.setSelectedIndex(2);
		libraryList.setBounds(100, 20, 200, 25);
		frame.add(libraryList);
	}

	
	static void addNewRow(Object[] row){
		tableModel.addRow(row);
	}
	
//----------------------------------------------------------------------------------------------------------------------
	
	
	
	@SuppressWarnings("serial")
	public static void main(String[] args) {
		
		frame = new JFrame();
		//JFrame.setDefaultLookAndFeelDecorated(true);
		frame.setBounds(0, 0, 700, 500);
		frame.setLayout(null);
		frame.setTitle("BookFriend " + versionNR);
		frame.setMinimumSize(new Dimension(700, 500));
		Container c = frame.getContentPane();
				c.setBackground(new Color(4, 41, 108));
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		int w = frame.getSize().width;
		int h = frame.getSize().height;
		int x = (dim.width-w)/2;
		int y = (dim.height-h)/2;
		
		frame.setLocation(x, y);
		
		
		
		
		

//----------------------------------------------------------
		showButtons();
//----------------------------------------	
	
		try {
			

	
		    table = new JTable();
		    String columnNames[] = {"", "ID", "Name", "Author", "Publisher", "Edition", "Year", "ISBN", "Status"};
		    tableModel = new DefaultTableModel(null, columnNames){
		    	public boolean isCellEditable(int rowIndex, int columnIndex) {
		    		columnIndexer = columnIndex;
		    		selectedRow = rowIndex;
		    		
		    		if (columnIndex !=0)
		  	  		  return false; 
		  	  	  return true;
		  	      }
		    };
		    
		    table.setModel(tableModel);

		    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		    table.setGridColor(Color.blue);
		    
			ListSelectionModel listSelectionModel = table.getSelectionModel();
			listSelectionModel.addListSelectionListener(new ListSelectionListener() {
				
				@Override
				public void valueChanged(ListSelectionEvent e) {
					ListSelectionModel lsm = (ListSelectionModel)e.getSource();
					bookRead.setEnabled(!lsm.isSelectionEmpty());
					
				}
			});
		    
			TableColumn includeColumn = table.getColumnModel().getColumn(0);
			includeColumn.setCellEditor(table.getDefaultEditor(Boolean.class));
			includeColumn.setCellRenderer(table.getDefaultRenderer(Boolean.class));
		    
			Object [] row = new Object[9];
			row[0] = new Boolean(false) ;
			row[1] = "1";
			row[2] = "book 1";
			row[3] = "Author 1";
			row[4] = "Publisher 1";
			row[5] = "First Edition";
			row[6] = "2001";
			row[7] = "1234567890";
			row[8] = "Stored";
			
			Object [] row2 = new Object[9];
			row2[0] = new Boolean(false) ;
			row2[1] = "2";
			row2[2] = "book 2";
			row2[3] = "Author 2";
			row2[4] = "Publisher 2";
			row2[5] = "First Edition";
			row2[6] = "2001";
			row2[7] = "1234567890";
			row2[8] = "Stored";
			
			for (x=0; x<25; x++){
				addNewRow(row);
				addNewRow(row2);
			}
			
		    TableColumn colSelect = table.getColumnModel().getColumn(0);
		    colSelect.setPreferredWidth(20);
		    
		    TableColumn colName = table.getColumnModel().getColumn(2);
		    colName.setPreferredWidth(170);
		    
		    TableColumn colAuthor = table.getColumnModel().getColumn(3);
		    colAuthor.setPreferredWidth(120);
		    
		    TableColumn colPublisher = table.getColumnModel().getColumn(4);
		    colPublisher.setPreferredWidth(120);
		    
		    table.removeColumn(table.getColumnModel().getColumn(1));
		    
		    
		    
		    
		     
	        //table.getTableHeader().setBackground( new Color(196, 216, 253) ); 
	        
		    scrollPane = new JScrollPane(table);
		    scrollPane.setBounds(100, 60, 590, 400);
		    frame.addComponentListener(new ComponentListener() {
				
				@Override
				public void componentShown(ComponentEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void componentResized(ComponentEvent e) {
					Rectangle rect = frame.getContentPane().getBounds();
					scrollPane.setBounds(100, 60, rect.width-110, rect.height-70);
					
				}
				
				@Override
				public void componentMoved(ComponentEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void componentHidden(ComponentEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
		    
		    //scrollPane.setBackground(new Color(196, 216, 253));  
	        //scrollPane.getViewport().setBackground(new Color(196, 216, 253)); 
	        
		    frame.add(scrollPane);
				
		    
		    
			table.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e)
	            {
					if (e.getClickCount() != 2){
						
							return;
					}	
					if (columnIndexer != 0)
						bInfo = new BookInfo();

				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		catch(Exception e) {
			e.printStackTrace();
		}
//---------------------------------------------


		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setVisible(true);
		
		
		
	}

}