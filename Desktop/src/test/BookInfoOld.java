package test;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;

import bookFriend.MainWindow;



public class BookInfoOld {

	/**
	 * @param args
	 */
	JFrame jFbook;	
	public String bID;
	public String bTitle;
	public JLabel label2;
	
	public void setMyTitle(String bTitle, String bID) 
	{
		jFbook.setTitle("Book Info - " + bTitle + " - " + bID);
		
	}
	
	public BookInfoOld() {
		jFbook = new JFrame();
		jFbook.setBounds(0, 0, 500, 400);
		jFbook.setLayout(null);
		jFbook.setMinimumSize(new Dimension(500, 400));
		
//		MainWindow mainWindow = new MainWindow();
		MainWindow.selectedRow = MainWindow.table.getSelectedRow();
			
		
		bID = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 1);
		bTitle = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 2);
		setMyTitle(bTitle, bID);
				
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = jFbook.getSize().width;
		int h = jFbook.getSize().height;
		int x = (dim.width-w)/2;
		int y = (dim.height-h)/2;
		jFbook.setLocation(x, y);
		
		
		JLabel label = new JLabel("hello");
		label.setBounds(100, 100, 200, 20);
		jFbook.add(label);
		
		int rowNr;
		int columnCount = MainWindow.table.getColumnCount();
		String cC = "";

		
		for(rowNr=0; rowNr < columnCount; rowNr++){
			Object o = MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, rowNr);
			cC += o.toString() + " ";
		}
		
//		String test = (String) MainWindow.table.getModel().getValueAt(MainWindow.selectedRow, 2);

		label2 = new JLabel(cC);
		label2.setBounds(10, 120, 400, 20);
		jFbook.add(label2);
		
		jFbook.setVisible(true);
		
	}

}


