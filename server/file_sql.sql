-- MySQL dump 10.13  Distrib 5.1.58, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: BookFriend
-- ------------------------------------------------------
-- Server version	5.1.58-1ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Books`
--

DROP TABLE IF EXISTS `Books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Books` (
  `checkbox` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `book_ID` int(11) NOT NULL AUTO_INCREMENT,
  `book_title` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `book_author` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `book_publisher` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `book_edition` int(2) NOT NULL,
  `book_published` date NOT NULL,
  `book_ISBN` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `book_status` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `book_borrower_loaner` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'The name of the person to whom you borrow a book to or lend a book from.',
  PRIMARY KEY (`book_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1 COMMENT='The books';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Books`
--

LOCK TABLES `Books` WRITE;
/*!40000 ALTER TABLE `Books` DISABLE KEYS */;
INSERT INTO `Books` VALUES (NULL,1,'book1','author1','publisher1',1,'2011-03-15','1234567890','Stored',NULL),(NULL,2,'book2','author2','publisher2',1,'2010-02-16','987654321','Stored',NULL),(NULL,8,'title','asujdh','sda',4,'2011-12-08','1242465','Borrowed',NULL),(NULL,9,'segfs','sdfg','dfrgds',1,'0000-00-00','2342','Stored',NULL),(NULL,38,'I have a new title','asujdh','sda',1,'2011-12-08','1242465','Borrowed',''),(NULL,36,'titlesdfghjkl','asujdh','sda',1,'2011-12-08','1242465','Borrowed','George'),(NULL,37,'title','asujdh','sda',1,'2011-12-08','1242465','Borrowed',''),(NULL,40,'title','asujdh','sda',1,'2011-12-08','1242465','Borrowed',''),(NULL,39,'title','asujdh','sda',1,'2011-12-08','1242465','Borrowed',''),(NULL,24,'skdj','soidhnvwoie','skldjbfojub',1,'2011-12-14','9793862','Stored',NULL),(NULL,25,'soldibnosadjbo','lisadhnosad','sdjbncoksa',1,'2011-12-15','2147483647','Stored',NULL),(NULL,26,'Bleak House','Charles Dickens','penguin',5,'2009-01-06','9870-00037-85673-97655','Stored',NULL),(NULL,27,'selihfwao','ossidfhwso','swohbfo',3,'2011-12-08','20983yr2098ey','Borrowed',NULL),(NULL,29,'zslkdvfh','sliidhnwesoikj','skjfdbvdsokj',1,'2011-12-21','pp94755398469','Borrowed',NULL),(NULL,30,'sldinh','spidknvols','soidbvln',1,'2011-12-15','2375429083','On Loan',NULL),('',31,'sadkicjibio','slkdbvcoikj','lsaiknbol',1,'2011-12-13','w2qe3rf','On Loan','lasdkbsoijunbdc'),(NULL,32,'lasdfhnl','sdgvsdr','wsrfwcs',1,'2011-12-15','235','Borrowed',NULL),(NULL,33,'sdflih','p&#537;wufvmpo','sdfvb',1,'2011-12-15','32423','Stored',NULL),(NULL,34,'dnvskol','sldkvnl','efvlih',1,'2011-12-14','dlkfhvdslk','Borrowed','dfgi'),(NULL,35,'Drept Parlamentar','Simeon Murgu','Cordial Lex',2,'2010-04-05','978-973-9480-66-6','Stored','');
/*!40000 ALTER TABLE `Books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Libraries`
--

DROP TABLE IF EXISTS `Libraries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Libraries` (
  `libr_ID` int(11) NOT NULL AUTO_INCREMENT,
  `libr_name` varchar(20) NOT NULL,
  PRIMARY KEY (`libr_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='The libraries available for the application';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Libraries`
--

LOCK TABLES `Libraries` WRITE;
/*!40000 ALTER TABLE `Libraries` DISABLE KEYS */;
INSERT INTO `Libraries` VALUES (1,'Home'),(2,'Work');
/*!40000 ALTER TABLE `Libraries` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-01-02 19:07:13
