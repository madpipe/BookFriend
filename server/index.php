<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Book Friend</title>
<script type="text/javascript" src="jquery-1.7.1.js"></script>
<script type="text/javascript" src="MainWindow.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	  $('#rowclick tr')
	    .filter(':has(:checkbox:checked)')
	    .addClass('selected')
	    .end()
	  .click(function(event) {
	    $(this).toggleClass('selected');
	    if (event.target.type !== 'checkbox') {
	      $(':checkbox', this).attr('checked', function() {
	        return !this.checked;
	      });
	    }
	  });
	});
</script>
<link rel="stylesheet" type="text/css" href="style.css"/>
<?php
require_once 'database.php';

$con = mysql_connect($server,$user,$pass);
mysql_connect($server,$user,$pass) or
die('Could not connect: ' . mysql_error()."<br>");
mysql_select_db($database_name, $con) or
die("Could not connect to database<br>");	

$query1 = "select * from Books";
$result=mysql_query($query1) or die(mysql_error());  	
?>
</head>

<body>
	<div id="top_bar">
		<a href="index.php" class="navigation_a"><div class="navigation_d">Home</div></a>
		<a href="downloads.html" class="navigation_a"><div class="navigation_d">Downloads</div></a>
	</div>
	
	<div id="main_controls">
		<button onclick="addBook();" title="Add a new book to your library"><img src="images/book_add.png" alt="add Book" /></button>
		<button onclick="borrowBook();" title="Borrow a book to someone"><img src="images/book_borrow.png" alt="borrow Book" /></button>
		<button onclick="lendBook();" title="Lend a book from someone"><img src="images/book_lend.png" alt="lend Book" /></button>
		<button onclick="showInfo();" title="View the info about the selected book(s)"><img src="images/book_open.png" alt="open Book" /></button>
		<br><br><br><br>
		<button onclick="deleteBook();" title="Delete the selected book(s)"value="delete"><img src="./images/book_delete.png"/></button>
	</div>
	
	<div id="header">
		<button style="float:left;" onclick="manageLibraries();" title="Manage your Libraries"><img src="images/library.png" alt="manage Libraries" /></button>
		<form style="float:left; margin-left: 20px; margin-top: 15px;" action="" id="library_select_form">
			<select name="libraryes" style="width: 130px;">
				<option value="home" id="location">Home</option>
				<option value="work" id="location">Work</option>
				<option value="workshop" id="location">Workshop</option>
				<option value="Andrei" id="person">Andrei</option>
				<option value="Claudiu" id="person">Claudiu</option>
			</select>
		</form>
		<h1 id="library_title" style="left:250px; position: absolute; margin-top: 10px; width:500px;">The library from your Home</h1>
	</div>
		
	<div id="container">
		<!-- Begin table column names -->
			<table>
				<tr>
					<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<th style="display: none;">Book ID</th>
					<th width="200">Book Title</th>
					<th width="150">Book Author</th>
					<th width="110">Book Publisher</th>
					<th width="90">Book Edition</th>
					<th width="110">Date Published</th>
					<th width="200">Book ISBN</th>
					<th width="100">Book Status</th>
				</tr>
			</table>
		<!-- End table column names -->
		<div id="scroll">
			<!-- Begin table content -->
			<form name="table_list" method="post">
			<table id="rowclick">
			<?php
				while($row=mysql_fetch_array($result)){
					echo '<tr id="'. $row['book_ID'] .'"><td>';
					echo '<input type="checkbox" name="books[]" value="' . $row['book_ID'] . '"/>';
					echo '</td><td id="' . $row['book_ID'] . '" style="display:none;">';
					echo $row['book_ID'];
					echo '</td><td width="200" style="text-align: left;">';
					echo $row['book_title'];
					echo '</td><td width="150" style="text-align: left;">';
					echo $row['book_author'];
					echo '</td><td width="110" style="text-align: left;">';
					echo $row['book_publisher'];
					echo '</td><td width="90">';
					echo $row['book_edition'];
					echo '</td><td width="110">';
					echo $row['book_published'];
					echo '</td><td width="200">';
					echo $row['book_ISBN'];
					echo '</td><td width="100">';
					echo $row['book_status'];
					echo "</td></tr>";
				}
				echo "</table>";
				mysql_close($con);

			?>
			
			</table>
			</form>
			<!-- End table content -->
		</div>
	</div>
</body>
</html>