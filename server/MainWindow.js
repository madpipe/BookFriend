function addBook()
{
	myWindow=window.open('addBookForm.php','Add a new book to your library','width=450, height=400,resizable=0, left=100,top=100,screenX=400,screenY=200');
	myWindow.focus();
}

function deleteBook()
{
	var yes = confirm("Are you shure you want to delete these books?");
	
	if(yes){
		table_list.setAttribute("action", "deleteBook.php");
		table_list.setAttribute("target", "_self");
		table_list.submit();
	}
}

function showInfo()
{
	myWindow=window.open('about:blank', 'infoPage', 'width=450, height=400, resizable=0, left=100,top=100,screenX=400,screenY=200');
	myWindow.focus();
	table_list.setAttribute("action", "gatherBooks.php");
	table_list.setAttribute("target", "infoPage");
	//table_list.setAttribute("onsubmit", "");
	table_list.submit();

}

function editBook()
{
	bookInfo.submit();
}