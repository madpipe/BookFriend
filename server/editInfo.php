<?php
require_once 'database.php';
if(isset($_POST['book_ID']))
	$book_ID = $_POST['book_ID'];

$con = mysql_connect($server,$user,$pass);
mysql_connect($server,$user,$pass) or
die('Could not connect: ' . mysql_error()."<br>");
mysql_select_db($database_name, $con) or
die("Could not connect to database<br>");

$get_current_info = "SELECT * FROM Books WHERE book_ID=$book_ID";

$result = mysql_query($get_current_info) or die("Query problem: " . mysql_error());

mysql_close();

$data = mysql_fetch_array($result);

$title = $data['book_title'];
$author = $data['book_author'];
$publisher = $data['book_publisher'];
$edition = $data['book_edition'];
$published= $data['book_published'];
$isbn = $data['book_ISBN'];
$status = $data['book_status'];
$book_borrower_loaner = $data['book_borrower_loaner'];



if(
isset($_POST['book_title'])&&
isset($_POST['book_author'])&&
isset($_POST['book_publisher'])&&
isset($_POST['book_edition'])&&
isset($_POST['SelectedDate'])&&
isset($_POST['book_ISBN'])&&
isset($_POST['book_status'])&&
isset($_POST['book_borrower_loaner'])
){

	$title = $_POST['book_title'];
	$author = $_POST['book_author'];
	$publisher = $_POST['book_publisher'];
	$edition = $_POST['book_edition'];
	$published = $_POST['SelectedDate'];
	$isbn = $_POST['book_ISBN'];
	$status = $_POST['book_status'];
	$book_borrower_loaner = $_POST['book_borrower_loaner'];

	if (empty($title))
	echo "Please insert the Book\'s Title!";
	else if (empty($author))
	echo "Please insert the Book\'s Author!";
	else if (empty($publisher))
	echo "Please insert the Book\'s Publisher!";
	else if ($published=="Select Date")
	echo "Please insert the Date the Book was published!";
	else if (empty($isbn))
	echo "Please insert the Book\'s ISBN!";
	else {
		
		$query_update_book = "UPDATE Books
SET book_title='$title', book_author='$author', book_publisher='$publisher', book_edition='$edition', 
book_published='$published', book_ISBN='$isbn', book_status='$status', book_borrower_loaner='$book_borrower_loaner'
WHERE book_ID=$book_ID;";
		
		$con = mysql_connect($server,$user,$pass);
		mysql_connect($server,$user,$pass) or
		die('Could not connect: ' . mysql_error()."<br>");
		mysql_select_db($database_name, $con) or
		die("Could not connect to database<br>");
		
		mysql_query($query_update_book) or
		die("Query problem: " . mysql_error());
		
		mysql_close();
		
		echo '<div style="margin-top: 150px; margin-bottom:250px; margin-left: 50px;">
		<span>The changes were saved successfuly! </span>
		<br><br>
		<span>Please refresh the main window!</span>
		</div>';
		
		
	}
}
	//-------------------------------------------------------------------------------
?>

<html>
<head>
  <script language="JavaScript" type="text/javascript">
  
/**************************************************************************************
  htmlDatePicker v0.1
  
  Copyright (c) 2005, Jason Powell
  All Rights Reserved

  Redistribution and use in source and binary forms, with or without modification, are 
    permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of 
      conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list 
      of conditions and the following disclaimer in the documentation and/or other materials 
      provided with the distribution.
    * Neither the name of the product nor the names of its contributors may be used to 
      endorse or promote products derived from this software without specific prior 
      written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
  OF THE POSSIBILITY OF SUCH DAMAGE.
  
  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

  
***************************************************************************************/
// User Changeable Vars
var HighlightToday  = true;    // use true or false to have the current day highlighted
var DisablePast    = false;    // use true or false to allow past dates to be selectable
// The month names in your native language can be substituted below
var MonthNames = new Array("January","February","March","April","May","June","July","August","September","October","November","December");

// Global Vars
var now = new Date();
var dest = null;
var ny = now.getFullYear(); // Today's Date
var nm = now.getMonth();
var nd = now.getDate();
var sy = 0; // currently Selected date
var sm = 0;
var sd = 0;
var y = now.getFullYear(); // Working Date
var m = now.getMonth();
var d = now.getDate();
var l = 0;
var t = 0;
var MonthLengths = new Array(31,28,31,30,31,30,31,31,30,31,30,31);

/*
  Function: GetDate(control)

  Arguments:
    control = ID of destination control
*/
function GetDate() {
  EnsureCalendarExists();
  DestroyCalendar();
  // One arguments is required, the rest are optional
  // First arguments must be the ID of the destination control
  if(arguments[0] == null || arguments[0] == "") {
    // arguments not defined, so display error and quit
    alert("ERROR: Destination control required in function call GetDate()");
    return;
  } else {
    // copy argument
    dest = arguments[0];
  }
  y = now.getFullYear();
  m = now.getMonth();
  d = now.getDate();
  sm = 0;
  sd = 0;
  sy = 0;
  var cdval = dest.value;
  if(/\d{1,2}.\d{1,2}.\d{4}/.test(dest.value)) {
    // element contains a date, so set the shown date
    var vParts = cdval.split("/"); // assume mm/dd/yyyy
    sm = vParts[0] - 1;
    sd = vParts[1];
    sy = vParts[2];
    m=sm;
    d=sd;
    y=sy;
  }
  
//  l = dest.offsetLeft; // + dest.offsetWidth;
//  t = dest.offsetTop - 125;   // Calendar is displayed 125 pixels above the destination element
//  if(t<0) { t=0; }      // or (somewhat) over top of it. ;)

  /* Calendar is displayed 125 pixels above the destination element
  or (somewhat) over top of it. ;)*/
  l = dest.offsetLeft + dest.offsetParent.offsetLeft;
  t = dest.offsetTop - 125;
  if(t < 0) t = 0; // >
  DrawCalendar();
}

/*
  function DestoryCalendar()
  
  Purpose: Destory any already drawn calendar so a new one can be drawn
*/
function DestroyCalendar() {
  var cal = document.getElementById("dpCalendar");
  if(cal != null) {
    cal.innerHTML = null;
    cal.style.display = "none";
  }
  return
}

function DrawCalendar() {
  DestroyCalendar();
  cal = document.getElementById("dpCalendar");
  cal.style.left = l + "px";
  cal.style.top = t + "px";
  
  var sCal = "<table><tr><td class=\"cellButton\"><a href=\"javascript: PrevMonth();\" title=\"Previous Month\">&lt;&lt;</a></td>"+
    "<td class=\"cellMonth\" width=\"80%\" colspan=\"5\">"+MonthNames[m]+" "+y+"</td>"+
    "<td class=\"cellButton\"><a href=\"javascript: NextMonth();\" title=\"Next Month\">&gt;&gt;</a></td></tr>"+
    "<tr><td>S</td><td>M</td><td>T</td><td>W</td><td>T</td><td>F</td><td>S</td></tr>";
  var wDay = 1;
  var wDate = new Date(y,m,wDay);
  if(isLeapYear(wDate)) {
    MonthLengths[1] = 29;
  } else {
    MonthLengths[1] = 28;
  }
  var dayclass = "";
  var isToday = false;
  for(var r=1; r<7; r++) {
    sCal = sCal + "<tr>";
    for(var c=0; c<7; c++) {
      var wDate = new Date(y,m,wDay);
      if(wDate.getDay() == c && wDay<=MonthLengths[m]) {
        if(wDate.getDate()==sd && wDate.getMonth()==sm && wDate.getFullYear()==sy) {
          dayclass = "cellSelected";
          isToday = true;  // only matters if the selected day IS today, otherwise ignored.
        } else if(wDate.getDate()==nd && wDate.getMonth()==nm && wDate.getFullYear()==ny && HighlightToday) {
          dayclass = "cellToday";
          isToday = true;
        } else {
          dayclass = "cellDay";
          isToday = false;
        }
        if(((now > wDate) && !DisablePast) || (now <= wDate) || isToday) { // >
          // user wants past dates selectable
          sCal = sCal + "<td class=\""+dayclass+"\"><a href=\"javascript: ReturnDay("+wDay+");\">"+wDay+"</a></td>";
        } else {
          // user wants past dates to be read only
          sCal = sCal + "<td class=\""+dayclass+"\">"+wDay+"</td>";
        }
        wDay++;
      } else {
        sCal = sCal + "<td class=\"unused\"></td>";
      }
    }
    sCal = sCal + "</tr>";
  }
  sCal = sCal + "<tr><td colspan=\"4\" class=\"unused\"></td><td colspan=\"3\" class=\"cellCancel\"><a href=\"javascript: DestroyCalendar();\">Cancel</a></td></tr></table>"
  cal.innerHTML = sCal; // works in FireFox, opera
  cal.style.display = "inline";
}

function PrevMonth() {
  m--;
  if(m==-1) {
    m = 11;
    y--;
  }
  DrawCalendar();
}

function NextMonth() {
  m++;
  if(m==12) {
    m = 0;
    y++;
  }
  DrawCalendar();
}

function ReturnDay(day) {
  cDest = document.getElementById(dest);
  //dest.value = (m+1)+"/"+day+"/"+y;
  dest.value = y+"-"+(m+1)+"-"+day;
  DestroyCalendar();
}

function EnsureCalendarExists() {
  if(document.getElementById("dpCalendar") == null) {
    var eCalendar = document.createElement("div");
    eCalendar.setAttribute("id", "dpCalendar");
    document.body.appendChild(eCalendar);
  }
}

function isLeapYear(dTest) {
  var y = dTest.getYear();
  var bReturn = false;
  
  if(y % 4 == 0) {
    if(y % 100 != 0) {
      bReturn = true;
    } else {
      if (y % 400 == 0) {
        bReturn = true;
      }
    }
  }
  
  return bReturn;
}  






  </script>
  <style type="text/css">
/**************************************************************************************
  htmlDatePicker CSS file
  
  Feel Free to change the fonts, sizes, borders, and colours of any of these elements
***************************************************************************************/
/* The containing DIV element for the Calendar */
#dpCalendar {
  display: none;          /* Important, do not change */
  position: absolute;        /* Important, do not change */
  background-color: #eeeeee;
  color: black;
  font-size: xx-small;
  font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
  width: 150px;
}
/* The table of the Calendar */
#dpCalendar table {
  border: 1px solid black;
  background-color: #eeeeee;
  color: black;
  font-size: xx-small;
  font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
  width: 100%;
}
/* The Next/Previous buttons */
#dpCalendar .cellButton {
  background-color: #ddddff;
  color: black;
}
/* The Month/Year title cell */
#dpCalendar .cellMonth {
  background-color: #ddddff;
  color: black;
  text-align: center;
}
/* Any regular day of the month cell */
#dpCalendar .cellDay {
  background-color: #ddddff;
  color: black;
  text-align: center;
}
/* The day of the month cell that is selected */
#dpCalendar .cellSelected {
  border: 1px solid red;
  background-color: #ffdddd;
  color: black;
  text-align: center;
}
/* The day of the month cell that is Today */
#dpCalendar .cellToday {
  background-color: #ddffdd;
  color: black;
  text-align: center;
}
/* Any cell in a month that is unused (ie: Not a Day in that month) */
#dpCalendar .unused {
  background-color: transparent;
  color: black;
}
/* The cancel button */
#dpCalendar .cellCancel {
  background-color: #cccccc;
  color: black;
  border: 1px solid black;
  text-align: center;
}
/* The clickable text inside the calendar */
#dpCalendar a {
  text-decoration: none;
  background-color: transparent;
  color: blue;
}  
  </style>
	<script type="text/javascript">
	
		function initStatus(){
			var status = "<?php echo $status;?>";
			switch (status) {
				case "Stored":
					//stored.setAttribute("selected", "selected");
					document.form1.book_status[0].selected = "1";
					break;
				case "Borrowed":
					//borrowed.setAttribute("selected", "selected");
					document.form1.book_status[1].selected = "1";
				break;
				case "On Loan":
					//onloan.setAttribute("selected", "selected");
					document.form1.book_status[2].selected = "1";
				break;
				default:
					break;
			}
		}
	</script>
</head>
<body style="overflow: hidden;" onload="initStatus(); setStatus(document.form1.book_status.value);">
<form method="post" action="editInfo.php" name="form1">
<div style="margin: 20px;">
<input type="text" name="book_ID" value="<?php echo $book_ID;?>" style="display: none;"/>
<span>Title</span>
<span style="position: absolute; left:250px;">Author</span><br>

<input type="text" name="book_title" value="<?php echo $title;?>" style="margin-left:10px;"/>
<input type="text" name="book_author" value="<?php echo $author;?>" 
style="position: absolute; left:250px; margin-left:10px;"/><br>
<br>

<span>Published Date</span>
<span style="position: absolute; left:250px;">Publisher</span><br>

<input type="text" name="SelectedDate" value="<?php echo $published;?>" id="SelectedDate" readonly onClick="GetDate(this);" style="margin-left:10px;"/>
<input type="text" name="book_publisher" value="<?php echo $publisher;?>" style="position: absolute; left:250px; margin-left:10px;"/><br>
<br>

<span>ISBN</span>
<span style="position: absolute; left:300px;">Edition</span><br>

<input type="text" name="book_ISBN" value="<?php echo $isbn;?>" style="margin-left:10px; width: 250px;"/>
<select name="book_edition" style="position: absolute; left:350px;margin-left:10px;">
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
</select><br>
<br>

<br>

<div id="status" style="border: 1px solid blue; padding: 15px; margin:-15px;">
<script type="text/javascript">
function setStatus(value){
	
	if(value == "Borrowed"){
		borrower.innerHTML = "Borrower";
		borrower.style.display = "block";
		book_borrower_loaner.style.display = "block";
	} else if (value == "On Loan"){
		borrower.innerHTML = "Loaner";
		borrower.style.display = "block";
		book_borrower_loaner.style.display = "block";
	} else {
		borrower.style.display = "none";
		book_borrower_loaner.style.display = "none";
	}
}
</script>
<span style="position:absolute; left: 250px;display:none;"id="borrower">Borrower</span>
<span>Status</span>


<br>
<input type="text" name="book_borrower_loaner" value="<?php echo $book_borrower_loaner;?>" id="book_borrower_loaner" 
style="position: absolute; left: 260px; display:none;"/>

<select name="book_status" onchange="setStatus(this.options[this.selectedIndex].value)" style="margin-left:10px; width: 100px; height: 25px;">
<option name="stored" value="Stored">Stored</option>
<option name="borrowed" value="Borrowed">Borrowed</option>
<option name="onloan" value="On Loan">On Loan</option>
</select>


</div>
<br>


<br>
<button onclick="window.close();" style="float: right; width:100px; height:40px;">Close</button>
<input type="submit" style="width: 100px; height: 40px;" value="Save"/>

</div>

</form>

</body>
</html>