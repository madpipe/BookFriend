package bookFriend.mobile;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class AddAuthor extends Activity implements OnClickListener{

	EditText authorName;
	DatePicker auth_birth, auth_deceased;
	TextView tvDeceased;
	ToggleButton deceased;
	Button save, cancel;
	AuthorList author = new AuthorList();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_author);
		initialize();
	}

	private void initialize() {
		authorName = (EditText) findViewById(R.id.authorName);
		auth_birth = (DatePicker) findViewById(R.id.auth_birth);
		auth_deceased = (DatePicker) findViewById(R.id.auth_deaceased);
		tvDeceased = (TextView) findViewById(R.id.textView2);
		deceased = (ToggleButton) findViewById(R.id.checkBox1);
		save = (Button) findViewById(R.id.saveAddAuthor);
		cancel = (Button) findViewById(R.id.cancelAddAuthor);
		
		tvDeceased.setVisibility(View.GONE);
		auth_deceased.setVisibility(View.GONE);
		deceased.setChecked(false);
		
		deceased.setOnClickListener(this);
		save.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.checkBox1:
			if(deceased.isChecked()){
				tvDeceased.setVisibility(View.VISIBLE);
				auth_deceased.setVisibility(View.VISIBLE);
			}
			else{
				tvDeceased.setVisibility(View.GONE);
				auth_deceased.setVisibility(View.GONE);
			}
			break;

		case R.id.saveAddAuthor:
			//author.authorsName.add(authorName.getText().toString());
			finish();
			break;
			
		case R.id.cancelAddAuthor:
			finish();
			break;
			
		default:
			break;
		}
	}

}
