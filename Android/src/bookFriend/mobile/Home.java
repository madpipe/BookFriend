package bookFriend.mobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Home extends Activity implements OnClickListener{

	Button bookList, authorList, publisherList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		initialize();
	}

	private void initialize() {
		bookList = (Button) findViewById(R.id.book_list);
		authorList = (Button) findViewById(R.id.author_list);
		publisherList = (Button) findViewById(R.id.publisher_list);
		
		bookList.setOnClickListener(this);
		authorList.setOnClickListener(this);
		publisherList.setOnClickListener(this);
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.book_list:
			Intent b = new Intent(Home.this, BookList.class);
			startActivity(b);
			break;
		case R.id.author_list:
			Intent a = new Intent(Home.this, AuthorList.class);
			startActivity(a);
			break;
		case R.id.publisher_list:
			Intent p = new Intent(Home.this, PublisherList.class);
			startActivity(p);
			break;
		default:
			break;
		}
	}

}
