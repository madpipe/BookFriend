package bookFriend.mobile;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class BookList extends ListActivity {
	private DAO dao;
	private List<Book> values;
	private ArrayAdapter<Book> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.book_list);

		dao = new DAO(this);
		dao.open();

		values = dao.getAllBooks();

		adapter = new ArrayAdapter<Book>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		Book book2 = values.get(position);

		 Bundle id1 = new Bundle();
		 id1.putInt("book_id", (int) book2.getBook_id());
		 Intent i = new Intent(BookList.this, BookInfo.class);
		 i.putExtras(id1);
		 startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater blowUp = getMenuInflater();
		blowUp.inflate(R.menu.book_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.addBook:
			Intent a = new Intent(BookList.this, AddBook.class);
			startActivity(a);
			break;
		case R.id.borrowBook:
			Intent b = new Intent(BookList.this, BorrowBook.class);
			startActivity(b);
			break;
		case R.id.preferences:
			// Intent p = new Intent();
			// startActivity(p);
			break;
		case R.id.exit:
			finish();
			break;
		default:
			break;
		}
		return false;
	}

	@Override
	protected void onResume() {
		dao.open();
		values.clear();
		values = dao.getAllBooks();
		adapter = new ArrayAdapter<Book>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
		super.onResume();
	}

	@Override
	protected void onPause() {
		dao.close();
		super.onPause();
	}

}
