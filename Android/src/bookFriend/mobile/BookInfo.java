package bookFriend.mobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class BookInfo extends Activity {

	private TextView bookTitle, bookAuthor, bookPublisher, bookEdition,
			bookPubDate, bookISBN, bookPersonName, statusTitle, status;
	private int book_id;
	private DAO dao;
	private Book book = new Book();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bookinfo);
		Bundle id = getIntent().getExtras();
		book_id = id.getInt("book_id");
		dao = new DAO(this);
		dao.open();
		book = dao.getBookById(book_id);
		initialize();

		setContents();
		dao.close();
	}

	protected void initialize() {
		bookTitle = (TextView) findViewById(R.id.bookTitle_i);
		bookAuthor = (TextView) findViewById(R.id.bookAuthor_i);
		bookPublisher = (TextView) findViewById(R.id.bookPublisher_i);
		bookEdition = (TextView) findViewById(R.id.bookEdition_i);
		bookPubDate = (TextView) findViewById(R.id.bookPubDate_i);
		bookISBN = (TextView) findViewById(R.id.bookISBN_i);
		bookPersonName = (TextView) findViewById(R.id.bookPersonName_i);
		statusTitle = (TextView) findViewById(R.id.statusTitle_i);
		status = (TextView) findViewById(R.id.bookStatus_i);
	}

	protected void setContents() {
		bookTitle.setText(book.getTitle());
		bookAuthor.setText(book.getAuthor());
		bookPublisher.setText(book.getPublisher());
		bookEdition.setText(Integer.toString(book.getEdition()));
		bookPubDate.setText(book.getPublishedDate());
		bookISBN.setText(book.getISBN());
		status.setText(book.getStatus());
		if (!book.getStatus().equalsIgnoreCase("Stored")) {
			bookPersonName.setVisibility(View.VISIBLE);
			statusTitle.setVisibility(View.VISIBLE);
			bookPersonName.setText(book.getPerson());
			statusTitle.setText("Person");
		} else {
			bookPersonName.setVisibility(View.GONE);
			statusTitle.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater blowUp = getMenuInflater();
		blowUp.inflate(R.menu.book_info_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.edit:
			Bundle id2 = new Bundle();
			id2.putInt("book_id", (int) book.getBook_id());
			Intent e = new Intent(BookInfo.this, EditBook.class);
			e.putExtras(id2);
			startActivity(e);
			break;
		case R.id.lend:
			
			break;
		case R.id.delete:
			AlertDialog.Builder alert = new AlertDialog.Builder(this);

			alert.setTitle("Delete book");
			alert.setMessage("Are you sure you want to delete the book " + book.getTitle() + "?");

			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dao.open();
				dao.deleteBook(book);
				dao.close();
				finish();
			  }
			});

			alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int whichButton) {
			    // Canceled.
			  }
			});

			alert.show();
			break;
		case R.id.cancelInfo:
			finish();
			break;
		}

		return false;
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		dao.open();
		book = dao.getBookById(book_id);
		setContents();
	}

}
