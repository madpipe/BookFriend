package bookFriend.mobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class EditBook extends Activity implements OnClickListener {

	private EditText bookTitle, bookAuthor, bookPublisher, bookEdition, bookPubDate,
			bookISBN, bookPersonName;
	private Spinner status;
	private Button save, cancel;
	private TextView statusTitle;
	private int book_id;
	private Book book = new Book();
	private DAO dao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_book);
		Bundle id = getIntent().getExtras();
		book_id = id.getInt("book_id");
		dao = new DAO(this);
		dao.open();
		book = dao.getBookById(book_id);
		initialize();
		setContents();
		dao.close();
	}

	private void initialize() {
		bookTitle = (EditText) findViewById(R.id.bookTitle_e);
		bookAuthor = (EditText) findViewById(R.id.bookAuthor_e);
		bookPublisher = (EditText) findViewById(R.id.bookPublisher_e);
		bookEdition = (EditText) findViewById(R.id.bookEdition_e);
		bookPubDate = (EditText) findViewById(R.id.bookPubDate_e);
		bookISBN = (EditText) findViewById(R.id.bookISBN_e);
		bookPersonName = (EditText) findViewById(R.id.bookPersonName_e);
		statusTitle = (TextView) findViewById(R.id.statusTitle_e);

		status = (Spinner) findViewById(R.id.spinner1_e);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter
				.createFromResource(this, R.array.status_list,
						android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		status.setAdapter(adapter);
		status.setSelection(0);
		status.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				String strSpinnerDay = status.getSelectedItem().toString();
				if (strSpinnerDay.equalsIgnoreCase("Stored")) {
					statusTitle.setVisibility(View.GONE);
					bookPersonName.setVisibility(View.GONE);
				} else if (strSpinnerDay.equalsIgnoreCase("On loan to someone")) {
					statusTitle.setVisibility(View.VISIBLE);
					statusTitle.setText("On loan to");
					bookPersonName.setVisibility(View.VISIBLE);
				} else if (strSpinnerDay
						.equalsIgnoreCase("Borrowed from someone")) {
					statusTitle.setVisibility(View.VISIBLE);
					statusTitle.setText("Borrowed from");
					bookPersonName.setVisibility(View.VISIBLE);
				}

			}

			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		save = (Button) findViewById(R.id.saveEdit);
		cancel = (Button) findViewById(R.id.cancelEdit);
		save.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}

	protected void setContents() {
		bookTitle.setText(book.getTitle());
		bookAuthor.setText(book.getAuthor());
		bookPublisher.setText(book.getPublisher());
		bookEdition.setText(Integer.toString(book.getEdition()));
		bookPubDate.setText(book.getPublishedDate());
		bookISBN.setText(book.getISBN());
		if(book.getStatus().equalsIgnoreCase("Stored")){
			status.setSelection(0);
			bookPersonName.setVisibility(View.GONE);
			statusTitle.setVisibility(View.GONE);
		}else if(book.getStatus().equalsIgnoreCase("Borrowed from someone")){
			status.setSelection(1);
			bookPersonName.setText(book.getPerson());
			statusTitle.setText("Person");
		}else if(book.getStatus().equalsIgnoreCase("On loan to someone")){
			status.setSelection(2);
			bookPersonName.setText(book.getPerson());
			statusTitle.setText("Person");
		}
	}
	
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.saveEdit:
			if (checkFields()) {
				setBookDetails();
				dao = new DAO(this);
				dao.open();
				dao.updateBook(book);
				dao.close();
				finish();
			} else {
				AlertDialog alertDialog = new AlertDialog.Builder(this)
						.create();
				alertDialog.setTitle("Field Empty");
				alertDialog.setMessage("All fields must be completed!");
				alertDialog.show();
			}
			break;
		case R.id.cancelEdit:
			finish();
			break;
		default:
			break;
		}
	}

	private void setBookDetails() {
		book.setTitle(bookTitle.getText().toString());
		book.setEdition(Integer.parseInt(bookEdition.getText().toString()));
		book.setPublishedDate(bookPubDate.getText().toString());
		book.setISBN(bookISBN.getText().toString());

		book.setAuthor(bookAuthor.getText().toString());
		book.setPublisher(bookPublisher.getText().toString());

		book.setStatus(status.getSelectedItem().toString());
		book.setPerson(bookPersonName.getText().toString());
	}

	private boolean checkFields() {
		if (bookTitle.getText().toString().trim().length() == 0)
			if (bookEdition.getText().toString().trim().length() == 0)
				if (bookPubDate.getText().toString().trim().length() == 0)
					if (bookISBN.getText().toString().trim().length() == 0)
						if (bookAuthor.getText().toString().trim().length() == 0)
							if (bookPublisher.getText().toString().trim()
									.length() == 0)
								if (status.getSelectedItemPosition() == 1
										&& bookPersonName.getText().toString()
												.trim().length() == 0)
									if (status.getSelectedItemPosition() == 2
											&& bookPersonName.getText()
													.toString().trim().length() == 0)
										return false;

		return true;
	}

}
