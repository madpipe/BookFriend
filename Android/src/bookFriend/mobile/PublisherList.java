package bookFriend.mobile;

import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PublisherList extends ListActivity {
	private DAO dao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.publisher_list);

		dao = new DAO(this);
		dao.open();

		List<String> values = dao.getAllPublishers();

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		// Bundle title_b = new Bundle();
		// title_b.putString("name", authorsName.get(position));
		// Intent i = new Intent(AuthorList.this, BookInfo.class);
		// i.putExtras(title_b);
		// startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater blowUp = getMenuInflater();
		blowUp.inflate(R.menu.publishers_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.preferences:

			break;
		case R.id.exit:
			finish();
			break;
		default:
			break;
		}
		return false;
	}

}
