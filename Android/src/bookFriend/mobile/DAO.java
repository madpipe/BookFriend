package bookFriend.mobile;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DAO {

	// Database fields
	private SQLiteDatabase sqLiteDb;
	private Database database;
	private String[] allColumns = { Database.COLUMN_ID,
			Database.COLUMN_BOOK_TITLE, Database.COLUMN_BOOK_EDITION,
			Database.COLUMN_BOOK_DATE, Database.COLUMN_BOOK_ISBN,
			Database.COLUMN_BOOK_STATUS, Database.COLUMN_PERSON_NAME,
			Database.COLUMN_AUTHOR_NAME, Database.COLUMN_PUBLISHER_NAME };

	public DAO(Context context) {
		database = new Database(context);
	}

	public void open() throws SQLException {
		sqLiteDb = database.getWritableDatabase();
	}

	public void close() {
		database.close();
	}

	public List<Book> getAllBooks() {
		List<Book> books = new ArrayList<Book>();
		Cursor cursor = sqLiteDb.query(Database.TABLE, allColumns, null, null,
				null, null, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			Book book = cursorToBook(cursor);
			books.add(book);
			cursor.moveToNext();
		}
		cursor.close();

		return books;
	}

	public List<String> getAllAuthors() {
		List<String> authors = new ArrayList<String>();
		Cursor cursor = sqLiteDb.query(true, Database.TABLE,
				new String[] { Database.COLUMN_AUTHOR_NAME }, null, null, null,
				null, Database.COLUMN_AUTHOR_NAME + " COLLATE NOCASE ", null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			authors.add(cursor.getString(0));
			cursor.moveToNext();
		}
		cursor.close();
		return authors;
	}
	
	public List<String> getAllPublishers() {
		List<String> publishers = new ArrayList<String>();
		Cursor cursor = sqLiteDb.query(true, Database.TABLE,
				new String[] { Database.COLUMN_PUBLISHER_NAME }, null, null, null,
				null, Database.COLUMN_PUBLISHER_NAME + " COLLATE NOCASE ", null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			publishers.add(cursor.getString(0));
			cursor.moveToNext();
		}
		cursor.close();
		return publishers;
	}

	public Book addBook(Book book_a) {
		ContentValues values = new ContentValues();
		values.put(Database.COLUMN_BOOK_TITLE, book_a.getTitle());
		values.put(Database.COLUMN_BOOK_EDITION, book_a.getEdition());
		values.put(Database.COLUMN_BOOK_DATE, book_a.getPublishedDate());
		values.put(Database.COLUMN_BOOK_ISBN, book_a.getISBN());
		values.put(Database.COLUMN_BOOK_STATUS, book_a.getStatus());
		values.put(Database.COLUMN_PERSON_NAME, book_a.getPerson());
		values.put(Database.COLUMN_AUTHOR_NAME, book_a.getAuthor());
		values.put(Database.COLUMN_PUBLISHER_NAME, book_a.getPublisher());

		long insertId = sqLiteDb.insert(Database.TABLE, null, values);

		Cursor cursor = sqLiteDb.query(Database.TABLE, allColumns,
				Database.COLUMN_ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();

		return cursorToBook(cursor);
	}
	
	public Book updateBook(Book book_u){
		ContentValues values = new ContentValues();
		values.put(Database.COLUMN_BOOK_TITLE, book_u.getTitle());
		values.put(Database.COLUMN_BOOK_EDITION, book_u.getEdition());
		values.put(Database.COLUMN_BOOK_DATE, book_u.getPublishedDate());
		values.put(Database.COLUMN_BOOK_ISBN, book_u.getISBN());
		values.put(Database.COLUMN_BOOK_STATUS, book_u.getStatus());
		values.put(Database.COLUMN_PERSON_NAME, book_u.getPerson());
		values.put(Database.COLUMN_AUTHOR_NAME, book_u.getAuthor());
		values.put(Database.COLUMN_PUBLISHER_NAME, book_u.getPublisher());

		sqLiteDb.update(Database.TABLE, values, "_id=?", new String[] {""+book_u.getBook_id()});

		return book_u;
	}

	public boolean deleteBook(Book book_d){
		sqLiteDb.delete(Database.TABLE, "_id=?", new String[] {""+book_d.getBook_id()});
		return true;
	}
	
	private Book cursorToBook(Cursor cursor) {
		Book book = new Book();
		book.setBook_id(cursor.getLong(0));
		book.setTitle(cursor.getString(1));
		book.setEdition(cursor.getInt(2));
		book.setPublishedDate(cursor.getString(3));
		book.setISBN(cursor.getString(4));
		book.setStatus(cursor.getString(5));
		book.setPerson(cursor.getString(6));
		book.setAuthor(cursor.getString(7));
		book.setPublisher(cursor.getString(8));

		return book;
	}

	public Book getBookById(int book_id) {
		Book book = new Book();
		Cursor cursor = sqLiteDb.query(Database.TABLE, allColumns, "_id=?", new String[] { ""+ book_id}, null, null, null);
		cursor.moveToFirst();
		book = cursorToBook(cursor);
		return book;
	}
}
