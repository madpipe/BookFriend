package bookFriend.mobile;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Database extends SQLiteOpenHelper{
	
	public static final String TABLE = "books";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_BOOK_TITLE = "b_title";
	public static final String COLUMN_BOOK_EDITION = "b_edition";
	public static final String COLUMN_BOOK_DATE = "b_date";
	public static final String COLUMN_BOOK_ISBN = "b_isbn";
	public static final String COLUMN_BOOK_STATUS = "b_status";
	public static final String COLUMN_PERSON_NAME = "b_person";
	public static final String COLUMN_AUTHOR_NAME = "a_name";
	public static final String COLUMN_PUBLISHER_NAME = "p_name";
	
	private static final String DATABASE_NAME = "bookFriend.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String DATABASE_CREATE = "create table " 
			+ TABLE + " ( " 
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_BOOK_TITLE + " text not null, "
			+ COLUMN_BOOK_EDITION + " integer, "
			+ COLUMN_BOOK_DATE + " text, "
			+ COLUMN_BOOK_ISBN + " text, "
			+ COLUMN_BOOK_STATUS + " text not null, "
			+ COLUMN_PERSON_NAME + " text, "
			+ COLUMN_AUTHOR_NAME + " text not null, "
			+ COLUMN_PUBLISHER_NAME + " text not null);";

	public Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(Database.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS" + TABLE);
		onCreate(db);
	}

}
