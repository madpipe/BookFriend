package bookFriend.mobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class AddBook extends Activity implements OnClickListener {

	EditText bookTitle, bookAuthor, bookPublisher, bookEdition, bookPubDate,
			bookISBN, bookPersonName;
	Spinner status;
	Button add, cancel;
	TextView statusTitle;
	private Book book = new Book();
	private DAO dao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_book);
		initialize();
		statusTitle.setVisibility(View.GONE);
		bookPersonName.setVisibility(View.GONE);
	}

	private void initialize() {
		bookTitle = (EditText) findViewById(R.id.bookTitle);
		bookAuthor = (EditText) findViewById(R.id.bookAuthor);
		bookPublisher = (EditText) findViewById(R.id.bookPublisher);
		bookEdition = (EditText) findViewById(R.id.bookEdition);
		bookPubDate = (EditText) findViewById(R.id.bookPubDate);
		bookISBN = (EditText) findViewById(R.id.bookISBN);
		bookPersonName = (EditText) findViewById(R.id.bookPersonName);
		statusTitle = (TextView) findViewById(R.id.statusTitle);

		status = (Spinner) findViewById(R.id.spinner1);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter
				.createFromResource(this, R.array.status_list,
						android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		status.setAdapter(adapter);
		status.setSelection(0);
		status.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				String strSpinnerDay = status.getSelectedItem().toString();
				if (strSpinnerDay.equalsIgnoreCase("Stored")) {
					statusTitle.setVisibility(View.GONE);
					bookPersonName.setVisibility(View.GONE);
				} else if (strSpinnerDay.equalsIgnoreCase("On loan to someone")) {
					statusTitle.setVisibility(View.VISIBLE);
					statusTitle.setText("On loan to");
					bookPersonName.setVisibility(View.VISIBLE);
				} else if (strSpinnerDay
						.equalsIgnoreCase("Borrowed from someone")) {
					statusTitle.setVisibility(View.VISIBLE);
					statusTitle.setText("Borrowed from");
					bookPersonName.setVisibility(View.VISIBLE);
				}

			}

			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		add = (Button) findViewById(R.id.saveAdd);
		cancel = (Button) findViewById(R.id.cancelAdd);
		add.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.saveAdd:
			if (checkFields()) {
				setBookDetails();
				dao = new DAO(this);
				dao.open();
				dao.addBook(book);
				dao.close();
				finish();
			} else {
				AlertDialog alertDialog = new AlertDialog.Builder(this)
						.create();
				alertDialog.setTitle("Field Empty");
				alertDialog.setMessage("All fields must be completed!");
				alertDialog.show();
			}
			break;
		case R.id.cancelAdd:
			finish();
			break;
		default:
			break;
		}
	}

	private void setBookDetails() {
		book.setTitle(bookTitle.getText().toString());
		book.setEdition(Integer.parseInt(bookEdition.getText().toString()));
		book.setPublishedDate(bookPubDate.getText().toString());
		book.setISBN(bookISBN.getText().toString());

		book.setAuthor(bookAuthor.getText().toString());
		book.setPublisher(bookPublisher.getText().toString());

		book.setStatus(status.getSelectedItem().toString());
		book.setPerson(bookPersonName.getText().toString());
	}

	private boolean checkFields() {
		if (bookTitle.getText().toString().trim().length() == 0)
			if (bookEdition.getText().toString().trim().length() == 0)
				if (bookPubDate.getText().toString().trim().length() == 0)
					if (bookISBN.getText().toString().trim().length() == 0)
						if (bookAuthor.getText().toString().trim().length() == 0)
							if (bookPublisher.getText().toString().trim()
									.length() == 0)
								if (status.getSelectedItemPosition() == 1
										&& bookPersonName.getText().toString()
												.trim().length() == 0)
									if (status.getSelectedItemPosition() == 2
											&& bookPersonName.getText()
													.toString().trim().length() == 0)
										return false;

		return true;
	}

}
